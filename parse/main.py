from parseLogs import parseLogs, TrialLog
import numpy as np
import matplotlib.pyplot as plt

printTrials = \
    [
    # 'data/noTail/',
    # 'data/oneTail/',
    # 'data/posing/',                 # Troubleshooting recording joint angles for a pose
    'data/posing2/',                # Posing with arms tucked to see
    'data/posingtest/',             # Posing with arms tucked to see
        #'data/oneTailFreeze2/',
    #'data/oneTailFreeze3/'
    ]

for thisDir in printTrials:
    thisTrial = parseLogs(thisDir)
    thisTrial.genplots()

plt.show()
