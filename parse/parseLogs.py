import csv
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# ------------------- Classes ------------------- #

def parseLogs(dirname):
    """Read in logs for each run on Krock.

    Currently doing this with printing out .txt files.
    Read in these separate .txt files and transform them into arrays (lists?).
    """

    timepath = dirname + 'timeLog.txt'
    anglepath = dirname + 'anglesLOG.txt'
    positionpath = dirname + 'positionLog.txt'
    torquepath = dirname + 'torqueLog.txt'
    forcepath = dirname + 'forceLOG.txt'  # not used yet
    fkinpath = dirname + 'fkinLog.txt'
    feetpath = dirname + 'feetReferenceLog.txt'


    # Read in file
    timeLog = np.genfromtxt(timepath, dtype=float, delimiter='\t')
    fKinLog = np.genfromtxt(fkinpath, dtype=float, delimiter='\t')
    cmdAnglesLog = np.genfromtxt(anglepath, dtype=float, delimiter='\t')
    forceLog = np.genfromtxt(forcepath, dtype=float, delimiter='\t')
    measAnglesLog = np.genfromtxt(positionpath, dtype=float, delimiter='\t')
    torqueLog = np.genfromtxt(torquepath, dtype=float, delimiter='\t')
    feetLog = np.genfromtxt(feetpath, dtype=float, delimiter='\t')

    # for row in cmdAnglesLog:
    #     print(row)

    # print cmdAnglesLog[0]


    print 'Time size: ', timeLog.shape
    print 'Force data size: ', forceLog.shape
    print 'Position data size: ', measAnglesLog.shape
    print 'Torque data size: ', torqueLog.shape
    print 'Angle data size: ', cmdAnglesLog.shape
    print 'Forward Kinematics data size: ', fKinLog.shape
    print 'Foot data data size: ', feetLog.shape

    # print 'Shape of column:', t.shape
    # print(forceLog[10,0])
    # print(type(forceLog[0, 0]))
    #

    thisTrial = TrialLog(timeLog, cmdAnglesLog, measAnglesLog, torqueLog, fKinLog, feetLog, dirname)

    return thisTrial


# ------------------- Classes ------------------- #

class TrialLog:
    """Data for a trial."""

    def __init__(self, time, cmdAngles, measAngles, curr, fkin, feet, dirname):
        """Simply store all relevant values.
        """
        self.t = time
        self.cmdAng = cmdAngles
        self.measAng = measAngles
        self.fKin = fkin
        self.curr = curr
        self.feet = feet
        self.dir = dirname

    def genplots(self):
        """Plot relevant data.

        Using this to streamline plotting for multiple trials.
        """

        plt.figure()

        xx = self.curr.shape
        nData = xx[1]

        plt.subplot(511)
        for ii in range(nData):
            plt.plot(self.cmdAng[:, ii])
        plt.ylabel('Commanded angles')
        plt.title(self.dir)

        plt.subplot(512)
        xx = self.fKin.shape
        for ii in range(xx[1]):
            plt.plot(self.fKin[:, ii])
        plt.ylabel('Forward Kinematics')

        plt.subplot(513)
        for ii in range(nData):
            plt.plot(self.measAng[:, ii])
        plt.ylabel('Measured Angle')

        plt.subplot(514)
        for ii in range(nData):
            plt.plot(self.curr[:, ii])
        plt.ylabel('Current')

        plt.subplot(515)
        yy = self.feet.shape
        nFootData = yy[1]
        plt.plot(self.feet[:,0])
        # print 'number of foot data: ', nFootData
        # for ii in range(nFootData):
        #     plt.plot(self.feet[:, ii])
        plt.ylabel('Foot Trajectories')
