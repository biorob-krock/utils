#ifndef __NETUTIL_H
#define __NETUTIL_H

/**
  * \file   netutil.h
  * \author Alessandro Crespi
  * \date   April 2009
  * \brief  Simple network utilities header file
  */

#include <cstdio>
#include <cstring>
#include <stdint.h>
#ifdef _WIN32
  #include <windows.h>
#else
  #include <arpa/inet.h>
  #include <netinet/in.h>
  #include <netinet/tcp.h>
  #include <netdb.h>
  #include <unistd.h>
#endif

uint32_t gethostaddress(const char* hn);
int block_recv(const int sk, uint8_t* data, unsigned int len);

#endif

