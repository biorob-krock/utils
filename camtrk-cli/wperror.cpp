#ifdef _WIN32
#include <windows.h>
#endif
#include <stdio.h>

/**
  * \file   wperror.cpp
  * \author Alessandro Crespi
  * \date   March 2009, updated March 2020
  * \brief  perror()-equivalent implementation for Win32
  */

void wperror(const char* str)
{
#ifdef _WIN32
  DWORD err = GetLastError();
  char* msg = NULL;
  
  FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | 60,
    NULL, err, LANG_NEUTRAL, (char*) &msg, 2048, NULL);
  fprintf(stderr, "%s: %s\n", str, msg);
  LocalFree(msg);
#else
  perror(str);
#endif
}
