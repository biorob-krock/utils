#include "netutil.h"

/**
  * \file   netutil.cpp
  * \author Alessandro Crespi
  * \date   April 2009
  * \brief  Simple network utilities
  */

uint32_t gethostaddress(const char* hn)
{
  struct hostent *hen;
  uint32_t addr;
  addr = inet_addr(hn);
  if (addr != INADDR_ANY && addr != INADDR_NONE) {
    return addr;
  }
  hen = gethostbyname(hn);
  if (hen != NULL) {
    if (sizeof(addr) != hen->h_length) {
      fprintf(stderr, "Internal error, address length mismatch.\n");
      _exit(2);
    }
    memcpy(&addr, hen->h_addr_list[0], hen->h_length);
    return addr;
  } else {
    return INADDR_NONE;
  }
}

// Receives a block of data of the specified size
int block_recv(const int sk, uint8_t* data, unsigned int len)
{
  int i, j;

  j = 0;

  while (len > 0) {
    i = recv(sk, (char*) data, len, 0);
    if (i==0 || i==-1) {
      return -2;
    }
    data += i;
    len -= i;
    j += i;
  }

  return j;
}
