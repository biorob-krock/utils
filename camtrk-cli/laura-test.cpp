/* #include <iostream>
#include <cstdlib>
#include <sys/time.h>
#include <unistd.h>
#include "trkcli.h"
#include <fstream>
using namespace std;


double
get_timestamp()
{
    struct timeval now;
    gettimeofday (&now, NULL);
    return  (now.tv_usec + (unsigned long long)now.tv_sec * 1000000)/1000000.;
}

int main()
{
  CTrackingClient trk;

  if (!trk.connect("biorobpc6.epfl.ch", 10502)) return 1;

  uint32_t last_time(0);
  static char buffer [80];
  std::ofstream leds;

  time_t rawtime;
  struct tm * timeinfo;
  time (&rawtime);
  timeinfo = localtime (&rawtime);
  strftime (buffer,80,"./data/leds_data_%Y-%m-%d_%H_%M_%S.txt",timeinfo);
  leds.open(buffer);

  while (true) {
    uint32_t t;
    if (!trk.update(t)) {
      cerr << "Failed to update points from the tracking server, quitting." << endl;
    }

    if (t != last_time) {
      double now = get_timestamp();
      const track_point* pt;
      int cnt;
      last_time = t;
      pt = trk.get_pos_table(cnt);
      cout.precision(3);
      cout << fixed<< now;
      leds<< fixed<< now;
      if (pt) {
        for (int i(0); i < cnt; i++) {
          cout << ",";
	        leds << ",";
          cout << pt[i].id << "," << pt[i].x << "," << pt[i].y;
	        leds<< pt[i].id << "," <<pt[i].x << "," << pt[i].y;
        }
      }
      cout << endl;
      leds<< endl;
    }
    usleep(1000);
  }
}


 */