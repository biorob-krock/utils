#ifndef __TRKCLI_H
#define __TRKCLI_H

/**
  * \file   trkcli.h
  * \author Alessandro Crespi
  * \date   April 2009, updated February 2020
  * \brief  Client class to retrieve data from the BIOROB LED tracking system 
  */

#include <stdint.h>

/// Entry containing the position of a detected point
struct track_point {
  int id;    ///< identifier of the point (not persistent across frames), starts from 100 for 2nd camera
  float x;   ///< X coordinate (in meters)
  float y;   ///< Y coordinate (in meters)
} __attribute__((__packed__));

const int MAX_POINTS = 40;

class CTrackingClient {

public:

  CTrackingClient();
  ~CTrackingClient();

  /// Connects to the specified tracking server
  bool connect(const char* hostname, u_short port = 10502);
  
  /// Starts a server-side log of all the tracking points
  bool start_tracking_file(const char* filename);
  /// Stops a previously started tracking log
  bool stop_tracking_file(void);
  
  /// \brief Retrieves the points detected in the current frame
  /// \param time The timestamp of the returned frame, in an undefined unit (DO NOT USE FOR TIMING)
  /// \return true if the operation succeeded, false in case of an error (e.g. lost connection)
  bool update(uint32_t& time);
  
  /// \brief Gets the identifier of the first detected point (useful when tracking a single point)
  /// \return The identifier of the point, or -1 if none are available
  int get_first_id();
  
  /// Extracts the coordinates of the given point (useful when tracking a single point)
  bool get_pos(const int id, double& x, double& y);
  
  /// \brief Gets a pointer to the table of all detected points in the last retrieved frame
  /// \param count Reference to an integer variable where the number of detected points will be written
  const track_point* get_pos_table(int& count) const;

private:

  int sock;
  bool connected;
  
  track_point positions[MAX_POINTS];
  int pos_count;

};

#endif
