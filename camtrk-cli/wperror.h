#ifndef __WPERROR_H
#define __WPERROR_H

/**
  * \file   wperror.h
  * \author Alessandro Crespi
  * \date   March 2009
  * \brief  perror()-equivalent implementation for Win32
  */

void wperror(const char* str);

#endif
