#include <iostream>
#include <cstdlib>
#include "termcolor.hpp"

/**
  * \file   test.cpp
  * \author Alessandro Crespi, Modified by Matt Estrada March 2020
  * \date   April 2009, updated March 2020
  * \brief  Example program to demonstrate the use of the tracking client class
  */

#ifdef _WIN32
#include <windows.h>
#define usleep(x) Sleep((x)/1000);
#else
#include <unistd.h>
#endif

#include "trkcli.h"
#include <sys/time.h>
#include <fstream>

using namespace std;

double
get_timestamp()
{
    struct timeval now;
    gettimeofday (&now, NULL);
    return  (now.tv_usec + (unsigned long long)now.tv_sec * 1000000)/1000000.;
}

int main()
{
  cout << termcolor::green << "\nStarting track_robot\n\n" << termcolor::reset << endl; 
  // declares an instance of the tracking client
  CTrackingClient trk;

  // Variables to write to file 
  static char buffer [80];
  std::ofstream leds;

  time_t rawtime;
  struct tm * timeinfo;
  time (&rawtime);
  timeinfo = localtime (&rawtime);
  strftime (buffer,80,"./data/camera_tracking_%Y-%m-%d_%H_%M_%S.txt",timeinfo);
  // leds.open(buffer);


  // connects to the server
  if (!trk.connect("biorobpc6.epfl.ch", 10502)) {
    return 1;
  }
  
  // starts a tracking server-side
  // if (!trk.start_tracking_file("example/test1.csv")) {
  //   return 2;
  // }
    static ofstream testwrite("./data/testing.txt");

  // displays the first detected point 10 times at 1-sec intervals
  for (int i =0; i<200; i++) {

      leds.open(buffer);

    uint32_t t;
    // updates the points from the tracking system
    if (!trk.update(t)) {
      cerr << "Failed to update points from the tracking server, quitting." << endl;
      return 3;
    }
    // gets the ID of the first point
    /*
    int id = trk.get_first_id();
    if (id != -1) {
      double x, y;
      // retrieves the x/y coordinates of that point
      if (trk.get_pos(id, x, y)) {
        cout << "pt " << id << ": " << x << ", " << y << "." << endl;
      }
    }
    */
    const track_point* pt;
    int cnt;
    pt = trk.get_pos_table(cnt);
    
    double now = get_timestamp();
    leds << fixed << now; 
    testwrite << fixed << now; 
    testwrite << "what" << now; 
    cout << fixed << now << endl; 

    if (pt) {
      cout << cnt << " points in table." << endl;

      for (int i(0); i < cnt; i++) {
          cout << ",";
	        leds << ",";
          cout << pt[i].id << "," << pt[i].x << "," << pt[i].y << endl;
	        leds<< pt[i].id << "," <<pt[i].x << "," << pt[i].y;
        }
    }
  leds.close();
    usleep(10000);

  }
  
  // stops the server-side tracking
  trk.stop_tracking_file();

  testwrite.close(); 
  // leds.close(); 
}
