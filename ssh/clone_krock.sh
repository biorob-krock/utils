#!/bin/bash
 
# THIS SCRIPT IS NOT MEANT TO BE RUN 
 
# Adapting it for Mac ports 
# pv is the Pipe Viewer package, showing the rate of data exchange 
# dd is terminal reading/writing 
 
TAR_DISK=/dev/sda
TAR_IMG=~/data/cloned-disks/krock_magenta_2020_04_11.img
 
# diskutil list will show you the ports 
diskutil list 
 
# Read disk 
sudo dd if=$TAR_DISK | pv | sudo dd of=$TAR_IMG bs=1024
 
# Common error is "resource busy" 
# Found solution here: 
# 	https://raspberrypi.stackexchange.com/questions/9217/resource-busy-error-when-using-dd-to-copy-disk-img-to-sd-cardcloneODROID.sh
 
# Write image
sudo dd if=$TAR_IMG | pv | sudo dd of=$TAR_DISK bs=1024
 
####################
# Notes from Ale on files to be changed after closing
####################
# Be sure to change these files 
# /etc/hostname (change)
# /etc/hosts (replace host -> hostname must resolve to 127.0.0.1, remove unrelated old hostnames to prevent conflicts)
# /etc/udev/rules.d/70-persistent-net.rules (EMPTY IT NO DELETE)
# /etc/network/interfaces